package main

import (
	"fmt"
	"os/exec"
	"os"
	"regexp"
	"bytes"
)

func main (){
	if(len(os.Args)==2){

		file_name:=os.Args[1];
		out,err:=exec.Command("objdump","-d",file_name).Output();
		if err!=nil{
			fmt.Println(err);
		}
		//fmt.Println(string(out));
		re:=regexp.MustCompile(`\t.*\t`);
		output:=re.FindAll(out,-1)
		var shellcode []byte;
		for _,v:=range output{
			fix1:=bytes.ReplaceAll(v,[]byte {0x9},nil)
			
			count:=0;
			siz:=0;
			for k,v1:=range fix1{
				if(v1==32){
					count++;
				}else{
					count=0;
				}
				if(count>1){
					siz=k-1;
					count=0;
					break;
				}
			}
			var final_line []byte;
			if(count==1){
				final_line=fix1[:20]
			}else{
				final_line=fix1[:siz]
			}
			
			var a []byte=[]byte{92,120}
			a=append(a,final_line...)
			final:=bytes.ReplaceAll(a,[]byte {0x20},[]byte("\\x"))
			shellcode=append(shellcode,final...);
		}
		fmt.Println(string(shellcode)); 




	}else{
		fmt.Println("usage: ./byte_extractor <filename>");
	}

}
//################################################NOTE##########################################
//please note that this program in no way is optimized . its purpose is purely for convinience after creating a shellcode and want to extract the bytes which can then be used for BOF